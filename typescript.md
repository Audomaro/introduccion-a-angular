- [Introducción a TypeScript](#introducci%C3%B3n-a-typescript)
  - [Ventajas y desventajas](#ventajas-y-desventajas)
  - [Porque debo aprenderlo](#porque-debo-aprenderlo)
  - [Instalación](#instalaci%C3%B3n)
  - [Compilación](#compilaci%C3%B3n)
  - [Tipos de datos](#tipos-de-datos)
    - [**`string`**](#string)
    - [**`number`**](#number)
    - [**`boolean`**](#boolean)
    - [**`Array`** y tupla](#array-y-tupla)
    - [**`enum`**](#enum)
    - [Object](#object)
    - [**`any`**](#any)
    - [Otros tipos de datos](#otros-tipos-de-datos)
  - [Uso de var, let y const](#uso-de-var-let-y-const)
  - [Operadores](#operadores)
    - [Operadores de asignación](#operadores-de-asignaci%C3%B3n)
    - [Operadores de comparación](#operadores-de-comparaci%C3%B3n)
    - [Operadores aritméticos](#operadores-aritm%C3%A9ticos)
    - [Operadores lógicos](#operadores-l%C3%B3gicos)
    - [Operador condicional (ternario)](#operador-condicional-ternario)
    - [Otros operadores](#otros-operadores)
  - [Estructuras de control](#estructuras-de-control)
    - [**`if ... else ...`**](#if--else)
    - [**`switch`**](#switch)
    - [**`while`**](#while)
    - [**`do ... while`**](#do--while)
    - [**`for`**](#for)
    - [**`for ... in`**](#for--in)
    - [**`for ... of`**](#for--of)
  - [Procedimientos y funciones](#procedimientos-y-funciones)
  - [POO](#poo)
    - [Modificadores](#modificadores)
    - [Clases](#clases)
    - [Campos y Propiedades](#campos-y-propiedades)
    - [Implementación de métodos](#implementaci%C3%B3n-de-m%C3%A9todos)
    - [Herencia](#herencia)
    - [Interfaces](#interfaces)
  - [Nombres de espacios](#nombres-de-espacios)

# Introducción a TypeScript

JavaScript es uno de los lenguajes más populares, en parte porque ha evolucionado y mejorado a pasos agigantados en los últimos años. Sin embargo, Javascript a pesar de ser un lenguaje flexible y no estructurado; lo que hace su aprendizaje rápido, no es fácil de escalar y mucho menos si un proyecto es grande.

Typescript apareció en el 2012 (luego de 2 años de desarrollo), fue una solución de Microsoft para el desarrollo de aplicaciones con Javascript a gran escala, para ellos y para sus clientes. Fue desarrollado por **Steve Lucco** y un equipo de más de 50 personas que incluía a **Anders Hejlsberg**; Lead Architect de C# y creador de Delphi y Turbo Pascal, originalmente el proyecto de TypeScript se le conoció como Strada.

Typescript es la solución a muchos de los problemas de JavaScript, está pensado para el desarrollo de aplicaciones robustas, implementando características en el lenguaje que nos permitan desarrollar herramientas más avanzadas para el desarrollo de aplicaciones.

Typescript es un superset de JavaScript, esto significa que los programas de JavaScript son programas válidos de TypeScript, a pesar de que TypeScript sea otro lenguaje de programación.

![Superset](superset.png)

> **Se dice superset a una tecnología de un lenguaje de programación, cuando puede ejecutar programas de la tecnología y del lenguaje del que es el superset.**

## Ventajas y desventajas

Ventajas:

- Es un lenguaje POO.
- Esta tipado, eso significa que la tipificación permite verificar la corrección del tipo en el momento de la compilación. Esto no está disponible en JavaScript.
- Señala los errores de compilación en el momento de desarrollo.
- Soporta módulos, clases e interfaces, así como funciones con parámetros opcionales.
- Soporta librerías creadas con JS.
- El código de TypeScript se convierte a JavaScript; se puede configurar a que version se va a compilar: desde ES3 a ES6.
- JavaScript es TypeScript, solo tenemos que cambiar el formato del archivo de **`.js`** a **`.ts`**

Desventajas:

- Se requiere un tiempo de compilación
- TypeScript no soporta clases abstractas.

## Porque debo aprenderlo

- Menos errores de código
- Mejora la calidad del software
- Esta respaldado por Microsoft
- Es un proyecto open source

## Instalación

El primer requisito para utilizar TypeScript es instalar Node.js. La forma mas conveniente de instalar TypeScript es a través de **`npm`**, el package manager que viene por default con Node.js. Para instalar abrimos una terminal y ejecutamos el comando:

```bash
npm install -g typescript
```

Para confirmar la correcta instalación y versión usamos el siguiente comando:

```bash
$ tsc --version
Version 3.0.3
```

## Compilación

Podemos compilar el código a JavaScript de diferentes maneras:

- A través de la terminal.
- De forma automática con **`Visual Studio 2015/2017`**.
- Con herramientas de building: <https://github.com/Microsoft/TypeScript/wiki/Integrating-with-Build-Tools>
  - Browserify
  - Duo
  - Grunt
  - Gulp
  - Jspm
  - Webpack
  - MSBuild
  - NuGet

Para compilar desde la terminal ejecutamos el comando:

```bash
tsc <archivo.ts>
```

Para compilar de forma automática cada que realicemos cambios, usamos la opción watch: **`-w`** o **`--watch`**.

```bash
tsc <archivo.ts> -w
```

## Tipos de datos

Typescript posee diferentes tipos de datos, entre los que encontramos:

### **`string`**

El tipo de dato **`string`** se utiliza para almacenar cadenas de texto. Tanto JavaScript como TypeScript usan comillas dobles ("), así como comillas simples (') para rodear una cadena. Una cadena puede contener cero o más caracteres entre comillas.

```ts
let saludo1: string = 'Hola mundo #1';
let saludo2 = 'Hola mundo #2'
```

### **`number`**

Este tipo se usa para representar tanto enteros como valores de punto flotante. Sin embargo, debe recordar que todos los números están representados internamente como valores de coma flotante. Los números también se pueden especificar como literales **`hexadecimales`**, **`octales`** o **`binarios`**.

Las representaciones **`octal`** y **`binaria`** se introdujeron en **`ES6`**, y esto puede dar como resultado una salida de código JavaScript diferente en función de la versión a la que esta compilando.

Además hay tres valores simbólicos especiales adicionales que se encuentran bajo el tipo de number: **`+Infinity`**, **`-Infinity`** y **`NaN`** (not a number).

```ts
let num1: number = 1.5;
let num2: number = 1;
let suma = num1 + num2;

let bin: number = 0b1;
let oct: number = 0o7;
let hex: number = 0xA;
```

### **`boolean`**

Boolean como se sabemos solo puede tener dos valores: **`true`** o **`false`**, y se utilizan mucho en estructuras de control.

```ts
let esValido: boolean;
esValido = true;
```

### **`Array`** y tupla

Un **`Array`** se puede definir de dos formas, la primera parecida a la declaración en otros lenguajes orientados a objetos:

```ts
let arregloA: number[];
arregloA = [10, 20, 30];
```

La segunda, mediante la declaración de un arreglo genérico:

```ts
let arregloB: Array<string>;
arregloB = ['a', 'b', 'c'];
```

El tipo tupla permite crear un array en donde el tipo y la cantidad de los elementos se conoce:

```ts
let tuple: [string, number];
tuple = ['Saludos', 0];

let tuple: [string, number, boolean] = ['Saludos', 0, false];
```

### **`enum`**

Este es un tipo de dato diseñado por el usuario, el cual le indica todos los posibles estados que puede tener una variable numérica mediante nombres. Por defecto un enum inicia en **`0`** pero se puede cambiar de forma manual.

```ts
enum EstadoUsuario {
  Desconectado = 10,
  Ausente = 20,
  Conectado = 30,
}
```

### Object

Cuando necesitemos que una variable tenga cierta estructura, podemos realizar una declaración de un objeto; con esto nos podemos evitar la declaración de una clase.

```ts
let persona: { nombre: string, edad: number, soltero: boolean } = {
  nombre: 'joe',
  edad: 10
};

let personas: { nombre: string, edad: number, soltero: boolean }[];
personas = [{
  nombre: 'pepito',
  edad: 10,
  soltero: true
}, {
  nombre: 'juanito',
  edad: 10,
  soltero: true
}];

let personas: Array<{ nombre: string, edad: number, soltero: boolean }>;
personas = [{
    nombre: 'pepito',
    edad: 10,
    soltero: true
}, {
    nombre: 'juanito',
    edad: 10,
    soltero: true
}];
```

### **`any`**

Cuando una variable pueda ser de cualquier tipo, cambie su tipo de forma dinámica o necesitemos un array formado por diferentes tipos, como una cadena de texto, número o booleano. Podemos utilizar este tipo de dato.

```ts
let miDato: any = 'Saludos';
console.log(miDato);

miDato = 20;
console.log(miDato);

miDato = false;
console.log(miDato);
```

### Otros tipos de datos

**`null`**, este tipo de dato solo puede tener un valor valido `null`.

**`undefined`** es el valor predeterminado de cualquier variable que no se haya asignado. Pero también se puede declara una variable con este, claro esta que el único valor que puede tener esta variable no es mas que **`undefined`**.

## Uso de var, let y const

El problema de **`var`** es que el scope que genera es confuso y generar resultados inesperados.

```ts
var n = 1;

for (var n = 0; n < 10; n++) {
  console.log(n);
}

console.log(`Valor final de n: ${n}`);
```

La diferencia de tiene **`let`**, es que trabaja solamente en el bloque en donde se declara.

```ts
let n = 1;

for (let n = 0; n < 10; n++) {
  console.log(n);
}

console.log(`Valor final de n: ${n}`);
```

**`const`** tiene un funcionamiento parecido a let, con la diferencia que el valor de una constante no puede cambiarse a través de la reasignación, y no se puede redeclarar.

```ts
const dias = 7;

dias = 10;

console.log(dias);
```

## Operadores

TypeScript usa los mismo operadores de JavaScript.

### Operadores de asignación

Un operador de asignación asigna un valor al operando de la izquierda en función del valor del operando de la derecha.

| Nombre                    | Operador abreviado | Significado |
| :-----------------------: | :----------------: | :---------: |
| Operadores de asignación  | x = y              | x = y       |
| Asignación de adición     | x += y             | x = x + y   |
| Asignación de sustracción | x -= y             | x = x - y   |

### Operadores de comparación

Un operador de comparación compara sus operandos y devuelve un valor lógico en función de si la comparación es verdadera (true) o falsa (false). Los operadores pueden ser numéricos, de cadena de caracteres (Strings), lógicos o de objetos. Las cadenas de caracteres son comparadas basándose en un orden lexicográfico estándar, usando valores Unicode. En la mayoría de los casos, si los dos operandos no son del mismo tipo, al igual que JavaScript, TypeScript intenta convertirlos en el tipo apropiado para permitir la comparación, generalmente esta conversión se realiza de manera numérica.

| Operando                       | Descripción                                                                                  | Ejemplo (true) |
| :----------------------------: | :------------------------------------------------------------------------------------------: | :------------: |
| Igualdad (==)                  | Devuelve true si ambos operandos son iguales.                                                | 3 == '3'       |
| Desigualdad (!=)               | Devuelve true si ambos operandos no son iguales.                                             | 4 != 3         |
| Estrictamente iguales (===)    | Devuelve true si los operandos son igual y tienen el mismo tipo.                             | 3 === 3        |
| Estrictamente desiguales (!==) | Devuelve true si los operandos no son iguales y/o no son del mismo tipo.                     | 3 !== '3'      |
| Mayor que (>)                  | Devuelve true si el operando de la izquierda es mayor que el operando de la derecha.         | 2 > 1          |
| Mayor o igual que (>=)         | Devuelve true si el operando de la izquierda es mayor o igual que el operando de la derecha. | 1 >= 1         |
| Menor que (<)                  | Devuelve true si el operando de la izquierda es menor que el operando de la derecha.         | 1 < 2          |
| Menor o igual que (<=)         | Devuelve true si el operando de la izquierda es menor o igual que el operando de la derecha. | 1 <= 5         |

### Operadores aritméticos

Los operadores aritméticos toman los valores numéricos (tanto literales como variables) de sus operandos y devuelven un único resultado numérico.

| Operador            | Descripción                                                                                                                                                                                                                            | Ejemplo                                                                                                                                  |
| :-----------------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------------------------------: |
| Resto (%)           | Operador binario correspondiente al módulo de una operación. Devuelve el resto de la división de dos operandos.                                                                                                                        | 12 % 5 devuelve 2.                                                                                                                       |
| Incremento (++)     | Operador unario. Incrementa en una unidad al operando. Si es usado antes del operando (++x) devuelve el valor del operando después de añadirle 1 y si se usa después del operando (x++) devuelve el valor de este antes de añadirle 1. | Si x es 3, entonces ++x establece x a 4 y devuelve 4, mientras que x++ devuelve 3 y, solo después de devolver el valor, establece x a 4. |
| Decremento (--)     | Operador unario. Resta una unidad al operando. Dependiendo de la posición con respecto al operando tiene el mismo comportamiento que el operador de incremento.                                                                        | Si x es 3, entonces --x establece x a 2 y devuelve 2, mientras que x-- devuelve 3 y, solo después de devolver el valor, establece x a 2. |
| Exponenciación (**) | Calcula la potencia de la base al valor del exponente. Es equivalente a base^(exponente)                                                                                                                                               | 2 \*\* 3 devuelve 8.10 \*\* -1 devuelve 0.1.                                                                                             |

### Operadores lógicos

Los operadores lógicos son comúnmente utilizados con valores booleanos; estos operadores devuelven un valor booleano. Sin embargo, los operadores && y || realmente devuelven el valor de uno de los operandos, asi que si estos operadores son usados con valores no booleanos, podrían devolverán un valor no booleano.

| Operador         | Uso              | Descripción                                                                          |
| :--------------: | :--------------: | :----------------------------------------------------------------------------------: |
| AND Lógico (&&)  | expr1 && expr2   | Devuelve true si ambos operandos son true, en caso contrario devuelve false.         |
| OR Lógico (\|\|) | expr1 \|\| expr2 | Devuelve true si alguno de los operandos es true, o false si ambos son false.        |
| NOT Lógico (!)   | !expr            | Devuelve false si su operando es true, en caso contrario, si es false devuelve true. |

### Operador condicional (ternario)

El operador condicional es el único operador de JavaScript que necesita tres operandos. El operador asigna uno de dos valores basado en una condición. La sintaxis de este operador es:

```ts
let adulto: string;
estado = (edad >= 18) ? 'Sí' : 'No';
```

### Otros operadores

- Operadores bit a bit
- Operadores de cadena de caracteres
- Operador de coma
- Operadores unarios
- Operadores relacionales

Más información sobre los operadores: <https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Expressions_and_Operators>

## Estructuras de control

Al igual que cualquier otro lenguaje, typescript hace uso de las estructuras de control con la cual tiene la posibilidad de realizar validaciones y repeticiones de instrucciones.

### **`if ... else ...`**

```ts
let esValido = true;

if(esValido) {
  // ...
} else {
  // ...
}
```

### **`switch`**

```ts
enum EstadoUsuario {
  Desconectado = 10,
  Ausente = 20,
  Conectado = 30,
}

let usuario: EstadoUsuario;

usuario = EstadoUsuario.Conectado;

switch(usuario) {
  case EstadoUsuario.Desconectado:
    // ...
    break;

    case EstadoUsuario.Ausente:
    // ...
    break;

    default:
    // ...
    break;
}
```

### **`while`**

```ts
let seguir: string = 'seguir';
let contador: number = 10;

while (seguir === 'seguir') {
  contador++;
  
  if (contador = 10) {
    seguir = '';
  }

  console.log(contador);
}
```

### **`do ... while`**

```ts
let contador: number = 0;

do {
  contador++;
} while(contador < 10);
```

### **`for`**

```ts
for(let i = 0; i < 100; i++) {
  // ...
}
```

### **`for ... in`**

```ts
let letras: string[] = ['a', 'b', 'c', 'd', 'e'];

for (let idx in letras) {
  console.log(idx);
}
```

### **`for ... of`**

```ts
let letras: string[] = ['a', 'b', 'c', 'd', 'e'];

for (let letra of letras) {
  console.log(letra);
}
```

## Procedimientos y funciones

La funciones en typescript son parecidas que las de javascript, con algunas diferencias:

- Parámetros tipados.

  ```ts
  function suma(a: number, b: number) {
    return a + b;
  }
  ```

- Uso de parámetros opcionales.

  ```ts
  function saludo(mensaje = 'Hola') {
    return mensaje;
  }
  
  console.log(saludo());
  ```

- Resultado tipado.

  ```ts
  function obtenerMenu(): string {
    return `
      === [ Opciones ] ===
      1. Listar
      2. Buscar
      3. Eliminar
      4. Modificar
      5. Salir
    `;
  }

  console.log(obtenerMenu());
  ```

- Funciones flecha (**`arrow`**)

  ```ts
  const funciones = {
      saludo: (mensaje = 'Hola') => {
          return mensaje;
      }
  }

  console.log(funciones.saludo());
  ```

- Operador Spread(**`...`**): este operador apareció con **`ES6`** y se utiliza para indicarle a un método o función el resto de los parámetros sin que estos sean definidos.

  Ejemplo de argumentos indefinidos en C#.

  ```cs
  static void Main(string[] args)
  {
    // ...
  }
  ```

  Implementación de Spread en TypeScript

  ```ts
  function imprimir(...datos: string[]): string {
    return `Datos: ${datos.join(', ')}`;
  }

  console.log(imprimir('Saludos'));
  console.log(imprimir('Uno', 'Dos'));
  console.log(imprimir('1', '2', '3'));

  /* Salida de la consola:

  Datos: Saludos
  Datos: Uno, Dos
  Datos: 1, 2, 3

  */
  ```

## POO

La programación orientada a objetos, es una tendencia de programación que basa la resolución de problemas, en la creación de los llamados objetos, que no son más que unidades que contienen una serie de características y atributos a los cuales se les asignará una serie de datos para resolver el problema.

Un objeto se compone de:

- Campos
- Constructor
- Propiedades
- Métodos (Es un procedimiento o una función, se le llama así debido a que pertenece a una clase)

### Modificadores

En TypeScript es importante el uso de los modificadores de los miembros. Ademas que estos no aplican para los mismos miembros:

Aplica para campos y métodos (procedimientos y funciones):

- **`private`**: limita a los miembros solo en clase en donde se declararon.
- **`protected`**: es parecido al anterior, con la ventaja que las clases derivadas pueden implementar los miembros declarados con este tipo.
- **`public`**: los miembros declaradores con este tipo, pueden ser usados por cualquiera. Este es el default para cualquier miembro;

Aplica solo para campos:

- **`readonly`**: los miembros declarados con este modificar solo pueden ser inicializados en el constructor y accedidos desde cualquier lugar.

Aplica solo para métodos:

- **`static`**: los métodos declarados con este tipo pueden ser implementados sin la necesidad de que la clase que los contiene sea instanciada.

### Clases

Es una plantilla para la creación de un objeto, contienen métodos y propiedades para el manejo adecuado de los datos. Cada objeto creado a partir de la clase se le llama instancia. La creación de una clase en typescript es sencilla, ejemplo:

```ts
class Persona {
  /// ...
}
```

### Campos y Propiedades

Los campos se utilizan para almacenar información del objeto, su estado o incluso estructuras más complejas, generalmente estos son privados y se modifican mediante las propiedades. Podemos declarar la clase de distintas formas:

```ts
/// tradicional, campos privados y propiedades publicas
class Persona {
  private _nombre: string;

  get getNombre: string {
    return this._nombre;
  }

  // no requiere ningún un tipo de dato.
  set setNombre(nombre: string) {
    this._nombre = nombre;
  }
}

// por medio del constructor y sin necesidad de propiedades
class Persona {
    private _nombre: string;

    constructor(public nombre: string) {
      this._nombre = nombre;
    }
  }
}

// Campos públicos
class Persona {
  public nombre: string;
}
```

### Implementación de métodos

La declaración de métodos es muy simple, ademas que esposible declarar el tipo al que pertenecen y el nivel de acceso.

```ts
class Persona {
  private _nombre: string;

  constructor() {
    this.respirar();
  }

  get getNombre(): string {
    return this._nombre;
  }

  // 👁: no requiere ningún un tipo de dato.
  set setNombre(nombre: string) {
    this._nombre = nombre;
  }

  private respirar(): void {
    console.log('Estoy respirando');
  }

  public caminar(): void {
    console.log('Estoy caminando');
  }

  public saludar(nombre: string): string {
    return `Hola ${nombre}, mi nombre es ${this.getNombre}`;
  }
}

let doctor = new Persona();
doctor.setNombre = 'Simi';
doctor.caminar();
console.log(doctor.saludar('Joe'));
```

### Herencia

Typescript también permite la implementación de la herencia, sin embargo, una clase no puede heredar de multiples clases.

```ts
class Persona {
  constructor(public nombre: string) { }

  public saludar() {
    console.log(`Hola, mi nombre es ${this.nombre}`);
  }
}

class Doctor extends Persona {
  constructor() {
    super('Joe');
    this.saludar();
  }
}

let john = new Doctor();
```

### Interfaces

La interfaz es una especie de contrato que una clase que la implementa debe de cumplir, ademas, el uso de estas permite que una clase implemente más de una interface.

```ts
interface Animal {
  nombre: string;
  comer(): void;
}

class Leon implements Animal {
  public nombre: string;

  public comer(): void {
    console.log('Comer');
  }
}

let leon = new Leon();
leon.nombre = 'Leon';
leon.comer();
```

## Nombres de espacios

Este nos permiten agrupar alguna lógica en trozos de código para que sean exportados y utilizados donde y cuando necesitemos, es un forma de crear aplicaciones escalables con cierto orden y un código fácil de mantener.

```ts
// Archivo #1
namespace Figuras {
  export interface ITipo {
    tipo(): void;
  }

  class Circulo implements ITipo {
    tipo(): void {
      console.log('Circulo');
    }
  }
  export function nombreModulo(): void {
    console.log('Figuras');
  };
}

// Archivo #2
namespace Figuras {
  export class Triangulo implements ITipo {
    public tipo() {
      console.log('Triangulo');
    }
  }

  export class Cuadrado implements ITipo {
    public tipo() {
      console.log('Cuadrado');
    }
  }
}

Figuras.nombreModulo();

let t = new Figuras.Triangulo();
t.tipo();

let c = new Figuras.Cuadrado();
c.tipo();
```
