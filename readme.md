- [Introducción a Angular](#introducci%C3%B3n-a-angular)
  - [SPA - Single Page Application](#spa---single-page-application)
  - [Ventajas de usar Angular](#ventajas-de-usar-angular)
  - [Instalación](#instalaci%C3%B3n)
  - [Estructura del directorio del proyecto](#estructura-del-directorio-del-proyecto)
    - [Contenido de la carpeta src](#contenido-de-la-carpeta-src)
    - [Contenido de la carpeta raíz](#contenido-de-la-carpeta-ra%C3%ADz)
  - [Elementos de angular](#elementos-de-angular)
    - [Concepto de modulo](#concepto-de-modulo)
    - [Concepto de componentes](#concepto-de-componentes)
    - [Concepto de plantillas](#concepto-de-plantillas)
    - [Concepto de decoradores y metadatos](#concepto-de-decoradores-y-metadatos)
    - [Concepto de servicios](#concepto-de-servicios)
    - [Concepto de directivas](#concepto-de-directivas)
    - [Arquitectura de angular](#arquitectura-de-angular)
  - [Librerías](#librer%C3%ADas)
  - [Modulo](#modulo)
  - [Componentes y plantillas](#componentes-y-plantillas)
    - [Crear un componente](#crear-un-componente)
    - [Hooks del ciclo de vida de un componente](#hooks-del-ciclo-de-vida-de-un-componente)
    - [Data Binding](#data-binding)
    - [Directivas](#directivas)
      - [Directivas de estructura](#directivas-de-estructura)
      - [Directivas de atributo](#directivas-de-atributo)
    - [Pipes](#pipes)
      - [Crear un pipe](#crear-un-pipe)
  - [El Routing en Angular](#el-routing-en-angular)
    - [Lista de rutas](#lista-de-rutas)
    - [Carga dinámica de componentes](#carga-din%C3%A1mica-de-componentes)
    - [Enlaces en el HTML](#enlaces-en-el-html)
    - [Recuperar parámetros](#recuperar-par%C3%A1metros)
  - [Servicios](#servicios)
  - [Formularios](#formularios)
    - [Diferencias claves](#diferencias-claves)
    - [Fundamentos comunes](#fundamentos-comunes)
    - [Configuración del modelo de formulario](#configuraci%C3%B3n-del-modelo-de-formulario)
      - [Configuración de Template-Drive Form](#configuraci%C3%B3n-de-template-drive-form)
      - [Configuración de Reactive Form](#configuraci%C3%B3n-de-reactive-form)
  - [Observables y programación reactiva](#observables-y-programaci%C3%B3n-reactiva)
    - [RxJS en Angular](#rxjs-en-angular)
    - [Observables](#observables)
    - [Suscripción](#suscripci%C3%B3n)
    - [Ejemplo](#ejemplo)
  - [Angular y consumo servicios web RESTful](#angular-y-consumo-servicios-web-restful)
    - [Qué es un servio web](#qu%C3%A9-es-un-servio-web)
    - [Que es un Api](#que-es-un-api)
    - [REST y RESTful](#rest-y-restful)
    - [Que hace que un servicio web sea REST](#que-hace-que-un-servicio-web-sea-rest)
    - [Consumir servicio webs](#consumir-servicio-webs)
      - [Instalación de IIS](#instalaci%C3%B3n-de-iis)
      - [Registrar webapi](#registrar-webapi)
      - [Consumo de webapi desde Angular](#consumo-de-webapi-desde-angular)
  - [Publicar una aplicación en IIS](#publicar-una-aplicaci%C3%B3n-en-iis)
    - [Configurar routing de IIS para aplicaciones Angular](#configurar-routing-de-iis-para-aplicaciones-angular)


# Introducción a Angular

Angular es un framework de desarrollo para JavaScript creado por Google, ademas es gratis y es opensource. Su objetivo es facilitarnos el desarrollo de aplicaciones web SPA y además darnos herramientas para trabajar con los elementos de una web de una manera más sencilla y optima.

> Algunos incluso llegan a decir de Angular que es lo que debería haber sido HTML si se hubiese pensado desde el principio para crear aplicaciones web, y no documentos.

Su primera versión, AngularJS, se convirtió en muy poco tiempo en el estándar de facto para el desarrollo de aplicaciones web avanzadas.

En septiembre de 2016 Google lanzó la versión definitiva de lo que llamó en su momento Angular 2, y que ahora es simplemente Angular. Este nuevo framework se ha construido sobre años de trabajo y feedback de los usuarios usando AngularJS (la versión 1.x del framework). Su desarrollo llevó más de 2 años. No se trata de una nueva versión o una evolución de AngularJS, sino que es un nuevo producto, con sus propios conceptos y técnicas. Además, Angular utiliza como lenguaje de programación principal TypeScript, un súper-conjunto de JavaScript/ECMAScript que facilita mucho el desarrollo.

## SPA - Single Page Application

Es un tipo de aplicación web donde todas las vistas se muestran en la misma página, sin recargar el navegador.

Técnicamente, una SPA es un sitio donde existe un único punto de entrada, generalmente el archivo index.html. En la aplicación no hay ningún otro archivo HTML al que se pueda acceder de manera separada y que nos muestre un contenido o parte de la aplicación, toda la acción se produce dentro del mismo index.html.

Aunque solo tengamos una página, lo que sí tenemos en la aplicación son varias vistas. En la misma página, por tanto, se irán intercambiando vistas distintas, produciendo el efecto de que tienes varias páginas, cuando realmente todo es la misma, intercambiando vistas.

![spa vs twa](imagenes/spa_twa.png)

## Ventajas de usar Angular

- **No me hagas pensar**: Angular posee varias librerías las cuales solo tenemos que implementar y desde la creación de un proyecto nos sugiere como debemos organizar el código. Ademas también nos ofrece el **`angular-cli`**, el cual es una herramienta que nos ofrece muchas posibilidades y tareas que con otros framework tendrías que programar.
- **TypeScript**: Aunque se podia programar con javascript puro, el equipo de Angular decidió utilizar TypeScript, por lo tanto, toda la documentación y ejemplos utilizan este lenguaje. Ademas el uso de este lenguaje permite que el desarrollo y mantenimiento de las aplicaciones a largo plazo se mucho más fácil.
- **Componentes Web**: Los componentes de Angular son **piezas de código que es posible reutilizar en otros proyectos sin mucho esfuerzo**, esto permite el desarrollo de aplicaciones más ágiles. Esto es debido a que el equipo de Angular adopto el estándar de componentes web, el cual es un conjunto de APIs que permiten crear etiquetas HTML personalizadas, reutilizables y auto-contenidas, que luego se pueden utilizar en otras páginas y aplicaciones web.Estos componentes personalizados funcionarán en navegadores modernos y con cualquier biblioteca o framework de JavaScript que funcione con HTML. Actualmente los navegadores no soportan directamente la implementación de estos, pero si por medio de **`polyfills`**
- **Futuro estable**: Un gran detalle de JavaScript son los cambios que tiene de manera frecuente, algunas librerías al que han intentado de seguirle el paso tardaron mucho en volverse estables. Sin embargo, detrás de Angular hay un equipo que toma decisiones meditadas y pausadas sobre el futuro, lo que evitara las prisas de 'yo también implementare eso'. Así que **podemos apostar que este es un framework a largo plazo**.
- **Gran soporte de herramientas**: Como las plantillas de Angular almacena por separado el código de la interfaz de usuario y el de la lógica de negocio, podemos sacarle provecho a muchas herramientas para este tipo de archivos, por ejemplo: Linters, plugins de Snippet, correctores, etc. Además, gracias a la popularidad de Angular, los principales editores e IDEs ofrecen ya extensiones para poder trabajar con este framework de la manera más cómoda posible.

## Instalación

Requisito inicial tenemos que tener instalado **Nodejs**, para comprobar la versión que tenemos instalada ejecutamos este comando en un shell o terminal (en windows se instala con Git):

```bash
$ node --version
v8.12.0
```

Para instalar Angular de forma global, ejecutamos el comando:

```bash
npm install -g @angular/cli
```

Una vez instalador, verificamos la instalación con:

```bash
ng --version
```

## Estructura del directorio del proyecto

Angular propone una estructura de organización de archivos.

> Para crear un proyecto usar **`ng new <nombre> --skip-tests --routing`**

```bash
my-app
├─── e2e
│     ├─── app.e2e-spec.ts
│     ├─── app.po.ts
│     └─── tsconfig.e2e.json
├─── node_modules
│     └─── ...
├─── src
│     ├─── assets
│     │     └─── .gitkeep
│     ├─── environments
│     │     ├─── environment.prod.ts
│     │     └─── environment.ts
│     ├─── app
│     │     ├─── app.module.ts
│     │     ├─── app.component.html
│     │     ├─── app.component.spec.ts
│     │     ├─── app.component.ts
│     │     └─── app.component.css
│     ├─── favicon.ico
│     ├─── index.html
│     ├─── main.ts
│     ├─── polyfills.ts
│     ├─── styles.css
│     ├─── test.ts
│     ├─── tsconfig.app.json
│     ├─── tsconfig.spec.json
│     └─── typings.d.ts
├─── karma.conf.js
├─── package.json
├─── protractor.conf.js
├─── README.md
├─── tsconfig.json
├─── tslint.json
├─── .angular-cli.json
├─── .editorconfig
└─── .gitignore
```

### Contenido de la carpeta src

| Archivo                                 | Descripción                                                                                                                                                                                                                                                                                   |
| --------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| assets/*                                | Este carpeta se utiliza para aquellos archivos que no son necesario compilar en la aplicación, por ejemplo: imágenes o fuentes de texto.                                                                                                                                                      |
| environments/*                          | Configuración de cada entorno de desarrollo, se usa por ejemplo: para incluir la url de los APIs REST, un token de aplicación, un id de la aplicación, etc. Esta configuración se usa cando la aplicación se compila para un entorno especifico.                                              |
| app/app.component.{ts,html,css,spec.ts} | Estos archivos conforman el componente raíz: **`AppComponent`**, el cual contendrá los componentes que creemos conforme avance nuestro proyecto. Esta formado un template HTML, una hoja **css**(less, scss), una lógica **ts** y un unit test **spec.ts**                                    |
| app/app.module.ts                       | Define el modulo raíz que sirve para indicar los elementos que componen la aplicación.                                                                                                                                                                                                        |
| favicon.icon                            | El icono que representa la aplicación web. ]                                                                                                                                                                                                                                                  |
| index.html                              | Es la página principal de la aplicación, por lo general no es necesario editar este archivo, ya que el *angular cli* de forma automática agrega todos los archivos **css** y **js** para que la aplicación funcione.                                                                          |
| main.ts                                 | Este es el archivo principal de la aplicación, permite que la aplicación se compile  **JIT** (**`ng build`**) y arrancar el modulo de esta, también podemos utilizar el método **AOT**(**`ng build -prod`**) agregando la bandera **`--aot`** a los comandos **`ng serve`** y **`ng serve`**. |
| polyfills.ts                            | Como actualmente los navegadores no tienen el mismo nivel para soportar algunas características, este archivo contiene varios polyfills que ayudan a normalizar estas diferencias, ademas es posible agregar nuestros propios polyfills si es necesario.                                      |
| styles.css                              | Contiene los estilos globales de la aplicación. También puede ser un archivo *less*, *css* o cualquier otro pre-procesador de *css* que soporte angular.                                                                                                                                      |
| test.ts                                 | Este es el archivo principal para los test de la aplicación, por lo general no es necesario editar.                                                                                                                                                                                           |
| tsconfig.{app\|spec}.json               | Configuración del compilador de TypeScript para la aplicación Angular (tsconfig.app.json) y para las pruebas unitarias (tsconfig.spec.json).                                                                                                                                                  |
| tslint.json                             | Configuración adicional para TSLint, se utiliza cuando se ejecuta **`ng lint`**. Un **Linter** es una herramienta que verifica las buenas practicas de un lenguaje de programación.                                                                                                           |
| typings.d.ts                            | Contiene las definiciones para TypeScript de librerías de terceros. Es necesario que se agreguen las definiciones de las librerias, por ejemplo: jQuery, si no se agregan el compilador de angular no podrá realizar el compilador debido a que no reconocerá la librería.                    |

### Contenido de la carpeta raíz

| Archivo            | Descripción                                                                                                                                                                                                                                                                                       |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| e2e/*              | Dentro de e2e / se encuentran las configuraciones para las pruebas de **extremo a extremo**. No deberían estar dentro de src / porque las pruebas e2e son en realidad una aplicación separada que simplemente prueba la aplicación principal. Por eso también tienen su propio tsconfig.e2e.json. |
| node_modules/*     | Node.js crea esta carpeta y coloca dentro de ella todos los módulos de terceros listados en package.json.                                                                                                                                                                                         |
| karma.conf.js      | Configuración para las pruebas                                                                                                                                                                                                                                                                    |
| package.json       | Configuración de npm que enumera los paquetes de terceros que utiliza el proyecto.                                                                                                                                                                                                                |
| protractor.conf.js | Configuración para las pruebas **extremo a extremo**, se usa cuando se ejecuta el comando **`ng e2e`**                                                                                                                                                                                            |
| README.md          | Documentación básica del proyecto, esta pre-cargada con información de los comandos de CLI. Se recomienda realizar la documentación.                                                                                                                                                              |
| tsconfig.json      | Configuración del compilador de TypeScript.                                                                                                                                                                                                                                                       |
| tslint.json        | Configuración adicional para TSLint, se utiliza cuando se ejecuta **`ng lint`**. Un **Linter** es una herramienta que verifica las buenas practicas de un lenguaje de programación.                                                                                                               |
| .angular-cli.json  | Configuración de angular cli para el proyecto.                                                                                                                                                                                                                                                    |
| .editorconfig      | Configuración simple del editor para asegurar que todos los que usan el proyecto tengan la misma configuración básica.                                                                                                                                                                            |
| .gitignore         | Configuración de **GIT** para asegurarse de que ciertos archivos generados automáticamente no estén comprometiendo el control del código fuente.                                                                                                                                                  |

## Elementos de angular

### Concepto de modulo

Un modulo es un conjunto de elementos de angular; componentes, servicios, directivas, etc, relacionados. También cuenta con la información de la configuración de una app.

### Concepto de componentes

Es un elemento que Angular estará renderizando en el HTML, la lógica se encuentra en una clase; la cual puede tener métodos y propiedades. El componente hace de mediador entre la vista de la plantilla y la lógica de la app donde estará el modelo de datos. Ademas el componente también incluirá una hoja de estilo la cual se utilizara para darle diseño.

### Concepto de plantillas

Las plantillas van a definir la vista de los componentes. Son archivos en HTML los cuales tienen sintaxis especial de Angular. Ademas con el databinding y las directivas, permite modificar o visualizar datos en pantalla.

### Concepto de decoradores y metadatos

Con los decoradores (patrón de diseño) con la que vamos a configurar dinamicamente atributos/metadatos de las clases y componentes.

Los metadatos van a describir como Angular usara las clases pero también describen relaciones que estos tienen con otros elementos, por ejemplo si tenemos un componente y una plantilla el metadato se va a encargar de decirle a Angular que ese componente y esa plantilla van juntos.

### Concepto de servicios

Los servicios son clases en la cual se definen funcionalidad que no esta relacionada con las vistas, su objetivo es la reutilización. Ademas se utilizan en los componentes, módulos u otros servicios mediante **DI - Dependency Injection**. Angular tiene sus propios servicios pero esto no impide que desarrollemos los nuestros.

### Concepto de directivas

Son funcionalidades que se aplican al **DOM**. Por ejemplo ejemplo con estos podemos indicar que un elemento de se muestre o no de acuerdo alguna validación, aplicar cierto estilo o interactuar con un modelo de datos.

En general son atributos que se aplican en cualquier parte de la platilla.

### Arquitectura de angular

![angular architecture](imagenes/angular_architecture.png)

## Librerías

Las librerías principales de Angular (siempre comienzan por **`@angular`**) son:

- **`@angular/core`**
- **`@angular/platform-browser`**
- **`@angular/router`**
- **`@angular/forms`**
- **`@angular/http`**

## Modulo

Un módulo de Angular, es un conjunto de código dedicado a un ámbito concreto de la aplicación, o una funcionalidad específica y se define mediante una clase decorada con **`@NgModule`**. Toda aplicación de Angular tiene al menos un módulo, el módulo principal (o root module).

> Un decorador agrega funcionalidad extra a una clase y condiciona el comportamiento de la misma por medio de los metadatos.

Los metadatos más importantes de un **`@NgModule`** son:

> Ver **`app.module`**

- **imports**: Otros NgModules, cuyas clases exportadas son requeridas por templates de componentes de este módulo.
- **declarations**: Las elementos que pertenecen al módulo. Hay 3 tipos de clases de tipos: componentes, directivas y pipes.
- **providers**: Los servicios que necesita el módulo, y que estarán disponibles para toda la aplicación.
- **bootstrap**: Define la vista raíz. Utilizado solo por el root module.
- **exports**: Conjunto de declaraciones que deben ser accesibles para templates de componentes de otros módulos.

## Componentes y plantillas

Un componente va a controlar un area de la vista. De hecho todo lo que se ve en una aplicación Angular es un componente. La lógica estará dentro de una clase la cual le dará soporte a la vista por medio de métodos y propiedades. El componente hace de mediador entre la vista a través de la plantilla y la lógica.

> Ver archivo **`app.component.ts`**

Los metadatos más importantes de un **`@Component`** son:

- **selector**: El selector de CSS que identifica que esta directiva en una plantilla y activa la creación de instancias de la directiva.
- **templateUrl**: La url de la platilla HTML que se vinculara al componente.
- **styleUrls**: Un array con urls de los archivos de estilos que queremos aplicar a nuestro componente.

Para probar la  aplicación ejecutamos el comando:

```bash
ng serve -o
```

> La aplicación se ejecuta de forma predeterminada en **`http://localhost:4200`**, 

### Crear un componente

Para crear un componente llamado **`Inicio`** usamos el siguiente comando:

```bash
ng generate component [<directorio/>]<nombre del componente>
```

también podemos usar:

```bash
ng g c [<directorio(opcional)>]<nombre del componente>
```

Ejemplo:

```bash
ng g c components/inicio
```

Para implementar el componente **`Inicio`** solo es necesario usar la etiqueta HTML que le corresponde en la plantilla del componente de **`App`**.

```html
<app-inicio></app-inicio>
```

### Hooks del ciclo de vida de un componente

Los componentes y directivas tienen un del ciclo de vida que Angular nos proporciona para poder interactuar con ellos en momentos claves de la existencia de estos objetos. Estos ganchos del ciclo de vida los podemos llamar como interfaces que son parte de la librería core de Angular.

![lifecycle hooks](imagenes/lifecycle_hooks.png)

| Interface           | metodo                | Descripción                                                                                                                                                                                                                                                                                     |
| ------------------- | --------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| OnChanges           | ngOnChanges           | Responde cuando Angular restablece propiedades de entrada vinculadas a datos. El método recibe un objeto **SimpleChanges** de los valores de propiedad actuales y anteriores. Este gancho es llamado antes de **ngOnInit** y cuando una o más propiedades de entrada enlazadas a datos cambien. |
| OnInit              | ngOnInit              | Inicializa la directiva o el componente una vez Angular haya mostrado las propiedades vinculadas a datos y establece las propiedades de entrada de la directiva o el componente. Es llamado solo una vez, después del primer llamado a **ngOnChanges**.                                         |
| DoCheck             | ngDoCheck             | Detecta y actúa sobre los cambios que Angular no puede por sí solo. Se llama durante cada detección de cambios, inmediatamente después de **ngOnChanges** y **ngOnInit**.                                                                                                                       |
| AfterContentInit    | ngAfterContentInit    | Es activado después de que Angular proyecte contenido externo en la vista del componente. Se llama una vez después del primer **ngDoCheck**. Este gancho de vida solo existe en los componentes.                                                                                                |
| AfterContentChecked | ngAfterContentChecked | Responde después de que Angular comprueba el contenido proyectado en el componente. Es llamado después del ngAfterContentInit y cada **ngDoCheck** posterior. Este gancho de vida solo existe en los componentes.                                                                               |
| AfterViewInit       | ngAfterViewInit       | Responde después de que Angular inicializa las vistas del componente y las vistas secundarias. Se llama una vez después del primer **ngAfterContentChecked**. Este gancho de vida solo existe en los componentes.                                                                               |
| AfterViewChecked    | ngAfterViewChecked    | Responde después de que Angular comprueba las vistas del componente y las vistas secundarias. Se llama después de **ngAfterViewInit** y cada **ngAfterContentChecked** posterior. Este gancho de vida solo existe en los componentes.                                                           |
| OnDestroy           | ngOnDestroy           | Es llamado justo antes de que Angular destruya la directiva o el componente. Anula la suscripción de observables y desconecta los manejadores de eventos para evitar fugas de memoria.                                                                                                          |

### Data Binding

Los “Data Bingdings” o enlace de datos es una técnica general que une una fuente de datos entre el origen de datos y las vista, y se encarga de sincronizarlos. Esto provoca que cada cambio de datos se refleje automáticamente en los elementos que están sincronizados.

Angular dispone de cuatro formas de data binding:

1. **Interpolación**: Angular se encarga de insertar el valor de un atributo desde el modelo de datos hacia el **DOM**.
2. **Property binding**: Aquí Angular por medio de un atributo del componente hijo un componente padre pasa un valor. Esto también es hacía el **DOM** o hacia el modelo. Un atributo de entrada usa el decorador **`@Input`**.
3. **Two-way binding**: Aquí Angular permite que las modificaciones sean desde o hacia el **DOM**, un ejemplo es el uso del **`ngModel`**.
4. **Event binding**: Por medio de un evento sobre el component Angular ejecuta una funciona. Para crear un evento personalizado se usa el decorador **`@Output`**.

![component databinding](imagenes/component_databinding.png)

### Directivas

Las directivas son básicamente funciones que son invocadas cuando el **DOM** es compilado por el framework. Se podría decir que las directivas están ligadas a sus correspondientes elementos del **DOM** cuando el documento es cargado.  La finalidad de una directiva es modificar o crear un comportamiento totalmente nuevo.

Una directiva se define como una clase que usa el decorador **`@Directive`**. Un Componente es una Directiva con template. De hecho **`@Component`** es un decorador **`@Directive`** extendido con características propias de los templates. Hay otros dos tipos de directivas, las estructurales y las atributo, y normalmente las vemos en forma de etiquetas de elementos HTML como atributos. Angular tiene sus propias directivas y también podemos crear nuestras propias directivas.

#### Directivas de estructura

Estas directivas, que se diferencian fácilmente al ser precedidas por un asterisco, alteran el layout añadiendo, eliminando o reemplazando elementos en el **DOM**.

- **`*ngIf`**: esta directiva toma una expresión booleana y hace que toda la porción del **DOM** aparezca o desaparezca dada esa condición.
  Ejemplo:

  ```ts
  public ver = true;
  ```

  ```html
  <p *ngIf="ver">Este es un párrafo que se ve</p>
  <p *ngIf="!ver">Este es un párrafo que no se ve</p>
  ```

- **`*ngFor`**: esta directiva es un poco más compleja que **`*ngIf`**, ya que permite generar iteraciones de elementos HTML. Posee partes obligatorias y opcionales. Las partes obligatorias:
  - Declaración de la variable que contiene el valor de la iteración
  - Utilización de palabra of.
  - Variable a iterar.

  Las partes opcionales:
  - Índice de la iteración.
  - Imprimir la variable que contiene el valor de la iteración con data binding.

  Ejemplo:

  ```ts
  public frutas = ['Manzanas', 'Peras', 'Platanos', 'Fresas'];
  ```

  ```html
  <h1>Lista de frutas</h1>
  <div *ngFor="let fruta of frutas; let i = index">
      <p>Indice {{i}} - Fruta {{fruta}}</p>
  </div>
  ```

- **`[ngSwitch]`**: esta directiva corresponde a una seria de directivas que cooperan entre si para generar un resultado. Estas directivas son **`ngSwitch`**, **`ngSwitchCase`** y **`ngSwitchDefault`**. La directiva **`ngSwitch`** es una directiva de atributo, mientras que las directivas **`ngSwitchCase`** y **`ngSwitchDefault`** corresponden a directivas estructurales.

  ```ts
  public verParrafo = 1;
  ```

  ```html
  <div [ngSwitch]="verParrafo">
    <p *ngSwitchCase="1">Párrafo #1</p>
    <p *ngSwitchCase="2">Párrafo #2</p>
    <p *ngSwitchDefault>Párrafo #3, se muestra por defecto.</p>
  </div>
  ```

- **`Directivas estructurales personalizadas`**:

  Para crear la base para la directiva personalizada, usamos el comando:

  ```bash
  ng generate direvtive [<directorio/>]<nombre de la directiva>
  ```

  o

  ```bash
  ng g d [<directorio/>]<nombre de la directiva>
  ```

  Ejemplo, vamos a crear la directiva IsAdmin:

  ```bash
  ng g d directives/IsAdmin
  ```

  Un vez creada es necesario agregar tres referencias en código en el constructor:
  - **Input**: permitirá pasar el valor que evaluara la directiva.
  - **TemplateRef**: Permite el acceso al contenido de la vista por medio del **`<ng-template>`**; el **`<ng-template>`** es un elemento angular para renderizar HTML. Nunca se muestra directamente. De hecho, antes de representar la vista, Angular reemplaza la **`<ng-template>`** y su contenido con un comentario.
  - **ViewContainerRef**: Permite el acceder al contenedor de la view, es decir, acceso al contenido.
  
  Ejemplo:

  ```ts
  import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core';

  @Directive({
    selector: '[appIsAdmin]'
  })
  export class IsAdminDirective {
    constructor(private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef) { }

    @Input()
    set appIsAdmin(condicion: boolean) {
      if (condicion) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      } else {
        this.viewContainer.clear();
      }
    }
  }
  ```

  ```html
  <div>
    <div *appIsAdmin="true">Si es administrador</div>
  </div>
  ```

#### Directivas de atributo

Las directivas atributo alteran la apariencia o el comportamiento de un elemento existente en el **DOM** y su apariencia es igual a la de atributos HTML.

- **`ngModel`**: Implementa un mecanismo de binding bi-direccional. El ejemplo típico es con el elemento HTML `<input>`, donde asigna la propiedad value a mostrar y además responde a eventos de modificación.

  ```html
  <input [(ngModel)]="nombre" name="nombre">
  ```

- **`ngClass`**: Esta directiva permite añadir/eliminar varias clases a un elemento de forma simultánea y dinámica.

  Ejemplo 1:

  ```ts
  public seleccionado = false;
  public rojo = true;
  ```

  ```html
  <div [ngClass]="{ 'seleccionado': seleccionado, 'rojo': rojo }">
    Div con clases dinámicas
  </div>
  ```

  Ejemplo 2:

  ```ts
  public clases() {
    let clases = {
      seleccionado: true,
      rojo: false
    };

    return setClases
  }
  ```

  ```html
  <div [ngClass]="clases()">
    Div con clases dinámicas
  </div>
  ```

- **`ngStyle`**: esta directiva permite asignar varios estilos en linea a un elemento.

  Ejemplo 1:

  ```ts
  public color = '#ff3388';
  ```

  ```html
  <div  [ngStyle]="{ 'background-color': 'yellow', color: 'black' }"></div>
  <div  [ngStyle]="{ 'background-color': color, color: 'white' }"></div>
  ```

  Ejemplo 2:

  ```ts
  private linea = false;
  private bold = true;

  public estilos() {
    let estilos = {
      'text-decoration': this.linea ? 'line-through' : 'none',
      'font-weight': this.bold ? 'bold' : 'normal',
    };

    return estilos;
  }
  ```

  ```html
  <div [ngStyle]="estilos()"></div>
  ```

- **`Directivas de atributo personalizadas`**:

  Para crearla, también se ejecuta el comando de crear directiva:

  ```bash
  ng g d directives/fondo
  ```

  Para realizar un cambio visual sobre el elemento es necesario agregar una referencia de **`ElementRef`**; esta referencia nos permitirá tener acceso a las propiedades de los estilos del mismo elemento y un **`Input`** para pasar el valor que se usara.

  Ejemplo:

  ```ts
  import { Directive, Input, ElementRef, HostListener, OnInit } from '@angular/core';

  @Directive({
    selector: '[appColorFondo]'
  })
  export class ColorFondoDirective implements OnInit {
    constructor(private element: ElementRef) { }

    @Input()
    appColorFondo: string;

    ngOnInit(): void {
      this.element.nativeElement.style.backgroundColor = this.appColorFondo;
    }
  }
  ```

  ```html
  <div appColorFondo="yellow">Seleccionado</div>
  ```

### Pipes

Los pipes o tuberías son pequeñas funciones que vamos a usar para formatear el resultado en la parte del HTML, la idea de estas son tomar un dato, hacer algo con este y dar un resultado.

Algunas de los pipes que podemos encontrar en Angular son:

- LowerCasePipe: convierte todo el texto a minúsculas. `<span>{{ 'HOLA' | lowercase }}</span>`
- UpperCasePipe: convierte todo el texto a mayúsculas. `<span>{{ 'hola' | uppercase }}</span>`
- TitleCasePipe: capitaliza la primera letra de cada palabra. `<span>{{ 'hola mundo' | titlecase }}</span>`
- CurrencyPipe: convierte un numero a formato moneda. `<span>{{ 20.44 | currency }}</span>`, `<span>{{ 20.44 | currency:'C' }}</span>`
- PercentPipe: convierte un numero en formato de porcentaje. `<span>{{ 25.05 | percent:'1.000' }}</span>`
- JsonPipe: convierte un objeto en formato json a cadena string.

  ```ts
  const obj = { a: 'valor a', b:'valor b' };
  ```

  ```html
  <span>{{ obj | json }}</span>
  ```

#### Crear un pipe

Crear un pipe personalizado para cortar texto y agregar puntos suspensivos, vamos a ejecutar el siguiente comando:

```bash
ng g p pipes/truncar
```

o

```bash
ng generate pipe pipes/truncar
```

Agregaremos el siguiente código en pipe:

```ts
/// ...
export class TruncarPipe implements PipeTransform {
  transform(cadena: any, limitar: string): any {
    const cortar = parseInt(limitar);
    return (cadena && cadena.length )> limitar ? cadena.substring(0, limitar) + '...' : cadena;
  }
}
```

Para implementarlo solo lo agregamos e indicamos el limite por medio del parámetro.

```html
<span>{{ 'Hola mundo' | trucar:4 }}</span>
```

> Ejercicio: crear el componente **`Navegacion`** con las opciones: Inicio y Tareas.

## El Routing en Angular

En cualquier sitio web tiene varias direcciones proporcionadas por el servidor, que sirven para mostrar distintos contenidos del sitio. Ya sea una portada, pagina de contactos, una lista de productos, configuración del perfil, etc. Cada una de las direcciones puede tener un HTML relacionado con el contenido.

En Angular solo tiene una entrada, **index.html** y toda la acción ocurre dentro de esta página. Tiene un componente principal sobre el cual se van cambiando las vistas.

Para facilitar la navegación en una aplicación de Angular se utiliza un sistema de **Routing**, que tiene el objetivo de permitir que en el sitio web haya rutas internas, respondiendo a rutas `virtuales` como las que existen en los sitios tradicionales.

Llamamos `virtuales` a esas rutas, porque realmente sólo existe un **index.html**, no habrá un archivo **contacto.html** o **clientes.html** para cada ruta, sino que será realmente siempre el **index.html** el que se entregue al navegador.

El sistema de routing es el encargado de reconocer cuál es la ruta que el usuario quiere mostrar, presentando la pantalla correcta en cada momento. Esto es útil por varios motivos, entre ellos:

- Permite que la aplicación responda a rutas internas. Es decir, no hace falta entrar siempre en la pantalla principal de la aplicación y navegar hasta la pantalla que queremos ver realmente.
- Permite que el usuario pueda usar el historial de navegación, asi puede ir hacia atrás y hacia adelante con el navegador para volver a una de las pantallas de aplicación que estaba viendo antes.

Ademas está compuesto por varios elementos que tienen que trabajar juntos para conseguir los objetivos planteados.

- El módulo del sistema de rutas: llamado **`RouterModule`**.
- Rutas de la aplicación: es un array con un listado de rutas que nuestra aplicación soportará.
- Contenedor: donde colocar las pantallas de cada ruta. Cada pantalla será representada por un componente.
- Enlaces de navegación: son enlaces HTML en los que incluiremos una directiva para indicar que deben funcionar usando el sistema de routing.

Cuando usamos la bandera de routing, el angular cli creo otro modulo llamado **AppRoutingModule** que cumple dos funciones. Por un lado importa al **RouterModule** de Angular, el cual contiene toda la lógica necesaria para enrutar en el navegador. Por otro lado, permite la definición de rutas en el array Routes[].

> Ver archivo app-routing.module.ts

### Lista de rutas

- **path**: se utiliza para indicar la dirección que resuelve. Podemos configurar de dos formas:
  - que resuelva una ruta especifica. Por ejemplo, `path: ''`, `path: 'perfil'`, ademas este puede incluir parámetros: `:parametro1/:parametro2/:parametroN`. Las rutas con parámetros no aceptaran ni más ni menos de los que se declararon.
  - que resuelva cual quier ruta que no hayamos definido, por ejemplo el clásica página `Error 400`.
    > Nota: las rutas no empiezan con '/'.
- **component**: aquí le indicamos que componente se mostrara al resolver la ruta.
- **redirectTo**(opcional): Se utiliza para redireccionar a otras rutas.
- **pathMatch**(opcional): La estrategia que se usara para hacer coincidir las rutas; estas pueden ser.
  - **`prefix`**(default): toma la primera ruta que coincida con el inicio de la URL, luego sigue buscando rutas secundarias donde coincide el resto de la URL.
  - **`full`**: significa que toda la ruta URL debe coincidir.

Ejemplo de array de ruteo:

```ts
const routes: Routes = [
  {
    path: 'inicio',
    component: InicioComponent,
    pathMatch: 'full'
  },
  {
    path: 'tarea',
    component: TareaComponent
  },
  {
    path: 'tarea/:id',
    component: FormComponent
  },
  {
    path: '400',
    component: e400Component
  },
  {
    path: '',
    redirectTo: 'inicio',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '400',
  }
];
```

### Carga dinámica de componentes

Para que sea utilizado el ruteo en el HTML se agrega la directiva **`router-outlet`**, esta directiva funciona como vista para la carga dinámica de los componentes según como sean llamados.

> Ver archivo app.component.html

### Enlaces en el HTML

Los enlaces web tradicionalmente se resuelven con elementos `<a href=""></a>` dónde en su atributo `href` tiene la dirección a la cuál navegar. En Angular los enlaces se declaran con un atributo especial llamado **`routerLink`**. Este atributo se compila dando lugar al href oportuno en tiempo de ejecución.

```html
<a routerLink="/">Inicio</a>
<a routerLink="/usuario">usuario</a>
<a routerLink="/usuario/20 ">usuario 20</a>
<a [routerLink]="['usuario', '20']">usuario 20</a>
```

### Recuperar parámetros

Podemos recuperar los parámetros enviados por medio de las rutas de distintas formas. En general se usa el objeto **`ActivatedRoute`**, el cual tiene detalles de la ruta entre los cuales se encuentran los parámetros.

```ts
import { ActivatedRoute, Params } from '@angular/router';
```

Después tendremos que inyectar la dependencia en el constructor:

```ts
constructor(private activatedRoute: ActivatedRoute) { }
```

Una forma de recuperar los parámetros es mediante la propiedad **`snapshot`**:

```ts
public ngOnInit(): void {
  console.log(this.activatedRoute.snapshot.params.id);
}
```

Otra forma es por medio del observable **`params`**, para lo cual es necesario usar una suscripción:

```ts
public ngOnInit(): void {
  this.activatedRoute.params.subscribe(res => console.log(res));
}
```

> Ejercicio, crear el componente **`Tareas`**, **`E400`**. Crear la ruta de **`Inicio`**,**`Tareas`** y también responder a la ruta raíz ('/') y responder a las rutas que no existan con el component **`E400`**.

## Servicios

Un servicio es un mecanismo para compartir funcionalidad entre los componentes, ademas esta funcionalidad debe ser especifica. Es ideal usarlos ya que los componentes se deben limitar a la experiencia de usuario, así como ofrecer métodos para consumir los servicios los cuales deben de llevar la carga de la lógica del negocio.

Los servicios se basa en el patron **singleton**, es decir, un servicio es una instancia única y por lo tanto tiene la ventaja de ser accedido desde cualquier punto de la aplicación.

Para crear un servicio se usa el comando:

```bash
ng generate service [<directorio/>]<nombre del servicio>
```

o

```bash
ng g s [<directorio/>]<nombre del servicio>
```

Para consumir un servicio se utiliza la inyección de dependencias, lo cual permite pasar una referencia a una instancia en diferentes componentes. Para inyectar una dependencia se debe importar y agregar al módulo principal (**`app.module`**) e indicar que es parte del provider; en la nueva versión de Angular de forma predeterminada el servicio pertenece al modulo **`root`**, esto se indica por medio del decorador **`@Injectable`** y el metadato **`providedIn`**:

Vamos a crear un módelo que represente el objeto **Tarea** y un servicio de **`Tareas`** con la siguiente funcionalidad:

- Guardar la lista de tareas.
- Llevar el control del id de tareas.
- Listar tareas.
- Agregar tareas.
- Modificar una tarea especifica.

Comando para crear el modelo:

```bash
ng g class models/tarea
```

Código del modelo:

```ts
export class Tarea {
  public id: number;
  public nombre: string;
  public descripcion: string;
  public terminada: string;

  constructor() {
    this.id = 0;
    this.nombre = '';
    this.descripcion = '';
    this.terminada = false;
  }
}
```

Comando para crear el servicio:

```bash
ng g s services/tareas
```

Código del servicio:

```ts
import { Injectable } from '@angular/core';
import { Tarea } from '../models/tarea';

@Injectable({
  providedIn: 'root'
})
export class TareasService {
  private lista: Tarea[] = [];
  private id = 0;

  constructor() {
    this.id++;
    this.lista = [{
      id: this.id,
      nombre: 'Demo',
      descripcion: 'Ejemplo',
      terminada: false
    }];
  }

  public listar(): Tarea[] {
    return this.lista;
  }

  public obtener(id: number): Tarea {
    return Object.assign({}, this.lista.find(t => t.id === id));
  }

  public agregar(tarea: Tarea): void {
    this.id++;
    tarea.id = this.id;
    this.lista.push(tarea);
  }

  public actulizar(tarea: Tarea): void {
    let sel = this.lista.find(t => t.id === tarea.id);
    sel = tarea;
  }
}
```

Ahora vamos a implementar el servicio en el componentes de **`Tareas`** y vamos a listar las tareas.

Código del componente:

```ts
import { Component, OnInit } from '@angular/core';
import { Tarea } from 'src/app/models/tarea';
import { TareasService } from 'src/app/services/tareas.service';

@Component({
 /// ...
 })
export class TareasComponent implements OnInit {
  public tareas: Tarea[];
  constructor(private tareasService: TareasService) { }

  ngOnInit() {
    this.tareas = this.tareasService.listar();
  }
}
```

Plantilla HTML:

```html
<h1>Listado</h1>

<table>
  <tr>
    <th>Id</th>
    <th>Nombre</th>
    <th>Detalles</th>
    <th>Terminado</th>
  </tr>
  <tr *ngFor="let tarea of tareas">
    <td>{{tarea.id}}</td>
    <td>{{tarea.nombre}}</td>
    <td>{{tarea.descripcion}}</td>
    <td>{{tarea.terminada}}</td>
  </tr>
</table>
```

## Formularios

Los formularios son la piedra angular en muchas aplicaciones, por medio de estos es posible iniciar sesión, actualizar datos, ingresar información confidencial, etc.

Angular proporciona dos formas de realizar formularios; cada uno procesa la información de manera distinta y tienen diferentes ventajas.

- Template-driven: son útiles cuando se requiere de un formulario sencillo, como para un formulario de registro de correos. Son fáciles de agregar a la aplicación, pero estos no son escalables, para esto es mejor usar Reactive Forms.
- Reactive: estos son mas robustos, más escalables, reusables y testeables. Si los formularios son una parte clave de la aplicación, entonces debemos usar este patron de formularios.

### Diferencias claves

La siguiente tabla resume las diferencias clave entre los Reactive Forms y Template-Drive Forms.

|                 | Template-Drive                           | Reactive                                     |
| :-------------: | :--------------------------------------: | :------------------------------------------: |
| Configuración   | Menos explicito,   creado por directivas | Más explicito, creado en clase de componente |
| Modelo de datos | Desestructura                            | Estructurado                                 |
| Previsibilidad  | Asíncrono                                | Sincrono                                     |
| Validaciones    | Por medio de directivas                  | Por medio de funciones                       |
| Mutabilidad     | Mutable                                  | Inmutable                                    |
| Escalabilidad   | Abstracción sobre APIs                   | Acceso a API de bajo nivel                   |

### Fundamentos comunes

Ambos tipos de formularios comparten bloques de construcción subyacentes.

- **`FormControl`**: rastrea el valor y el estado de validación de un control individual.
- **`FormGroup`**: rastrea los mismos valores y estado para una colección de controles de formulario.
- **`FormArray`**: rastrea los mismos valores y estado para una matriz de controles de formulario.
- **`ControlValueAccessor`**: crea un puente entre las instancias de Angular **`FormControl`** y los elementos DOM nativos.

### Configuración del modelo de formulario

Ambos patrones de formularios utilizan un modelo de formulario para rastrear los cambios de valor de los formularios.

#### Configuración de Template-Drive Form

Para comprender este tipo de formulario haremos lo siguiente:

- Crear un componente que representara el formulario de captura de una tarea.
- Use **`ngModel`** para crear enlaces de datos bidireccionales para leer y escribir valores de control de entrada.
- Rastrear los cambios y validez de los controles.
- Implemetar detalles visuales en base el estado de un control.
- Deshabilitar un botón en base a la validación del formulario.

Es importante que para el uso formularios la importación del modulo **`FormsModule`** en el modulo principal **`app.module.ts`**:

```ts
/// ...
import { FormsModule } from '@angular/forms';
/// ...
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
/// ...
```

Ahora vamos a crear el componente que representara el formulario:

```bash
ng g c components/TareaForm
```

Clase:

```ts
/// ...
export class FormTareaComponent implements OnInit {

  public tarea;

  constructor() { }

  ngOnInit() {
  }

  public guardar(e: any): void {
  }
}
```

HTML:

```html
<form (submit)="guardar($event)" #form="ngForm">
  Nombre: <input type="text" [(ngModel)]="tarea.nombre" #nombre="ngModel" name="nombre" required>
  <span [hidden]="nombre.valid" class="required-msj">Requerido</span>
  <br>
  <br>
  Descripción: <input type="text" [(ngModel)]="tarea.descripcion" name="descripcion">
  <br>
  <br>
  <button (click)="cancelar()">Cancelar</button>
  &nbsp;
  <button type="submit" [disabled]="!form.valid">Guardar</button>
</form>
```

Estilo:

```css
input {
  padding: 5px;
}

.ng-valid[required],
.ng-valid.required {
  border-bottom: 5px solid green;
}

.ng-invalid:not(form) {
  border-bottom: 5px solid red;
}

.required-msj {
  padding-left: 10px;
  font-weight: bold;
  color: red;
}
```

Lo siguiente es consumir el servicio de **Tareas**, así como **Guardar** o **Cancelar** la modificación de una tarea.

Código:

```ts
/// ...
  constructor(private activatedRoute: ActivatedRoute, private router: Router, private tareaServices: TareasService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((param: any) => {
      const id = parseInt(param.id, 10);
      this.tarea = new Tarea();

      if (id !== 0) {
        this.tarea = this.tareaServices.obtener(parseInt(param.id, 10));
      }
    });
  }

  public guardar(e: any): void {
    if (!this.tarea.id) {
      this.tareaServices.agregar(this.tarea);
    } else {
      this.tareaServices.actulizar(this.tarea);
    }

    this.cancelar();
  }

  public cancelar(): void {
    this.router.navigate(['tareas']);
  }
/// ...
```

Lo siguiente es agregar el acceso a este componente mediante el **`routerLink`**.

Plantilla del componente de **Tareas**:

```html
<button [routerLink]="[0]">Crear</button>

<table>
  <tr>
    <th>Id</th>
    <th>Nombre</th>
    <th>Detalles</th>
    <th>Terminado</th>
    <th></th>
  </tr>
  <tr *ngFor="let tarea of tareas">
    <td>{{tarea.id}}</td>
    <td>{{tarea.nombre}}</td>
    <td>{{tarea.descripcion | truncar: 15 }}</td>
    <td>
      <div class="slide" (click)="marcar(tarea)">
        <input type="checkbox" [checked]="tarea.terminada">
        <label></label>
      </div>
    </td>
    <td>
      <a [routerLink]="['/',tarea.id]">Editar</a>
    </td>
  </tr>
</table>
```

css:

```css
table {
  border-spacing: 5px;
}

td,
th {
  border: 1px solid black;
  background-color: whitesmoke;
  min-width: 150px;
  padding: 5px;
}

.slide {
  position: relative;
  width: 50px;
  height: 24px;
  background-color: rgb(207, 207, 207);
  border: 1px solid #333;
  cursor: pointer;
  user-select: none;
  font-weight: bold;
  box-sizing: border-box;
  padding: 3px;
  border-radius: 50px;
}

.slide label {
  position: absolute;
  top: 2px;
  left: 2px;
  width: 16px;
  height: 16px;
  background-color: red;
  font-weight: bold;
  transition: all .3s ease;
  border-radius: 16px;
  border: 1px solid #333;
}

.slide > * {
  cursor: pointer;
}

.slide input[type=checkbox] {
  visibility: hidden;
}

.slide input[type=checkbox]:checked+label {
  left: 28px;
  background-color: greenyellow;
}
```

#### Configuración de Reactive Form

Para el ejemplo haremos el mismo formulario anterior pero ahora usando Reactive Forms, para comenzar tenemos que agregar el modulo de Reactive Form a modulo root:

```ts
/// ...
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// ...

/// ...
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
/// ...
```

Ahora crearemos el componete de nuestro formulario su nombre sera **`FormTareaR`**, para esto usaremos:

```bash
ng g c components/FormTareaR
```

Una vez creado nuestro componente crearemos nuestro formulario usando los objetos: **`FormGroup`** y **`FormControl`**, tambien agregaremos las mismas funciones con algunos ajustes:

```ts
/// ...
import { FormGroup, FormControl, Validators } from '@angular/forms';
/// ...

@Component({
/// ...
})
export class FormTareaRComponent implements OnInit {
  public tareaForm = new FormGroup({
    id: new FormControl(0),
    nombre: new FormControl('12', [Validators.required]),
    descripcion: new FormControl('23'),
  });

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private tareaServices: TareasService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((param: any) => {
      const id = parseInt(param.id, 10);
      this.tarea = new Tarea();

      if (id !== 0) {
        const tarea = this.tareaServices.obtener(parseInt(param.id, 10));
      }

      this.tareaForm.controls['id'].setValue(tarea.id);
      this.tareaForm.controls['nombre'].setValue(tarea.nombre);
      this.tareaForm.controls['descripcion'].setValue(tarea.descripcion);
    });
  }

  public guardar(e: any): void {
    if (!this.tareaForm.get('id').value) {
      this.tareaServices.agregar(this.tareaForm.value);
    } else {
      this.tareaServices.actulizar(this.tareaForm.value);
    }

    this.cancelar();
  }

  public cancelar(): void {
    this.router.navigate(['tareas']);
  }

}
```

Usaremos el mismo HTML del formulario y también realizaremos algunos cambios:

```html
<form (submit)="guardar($event)" [formGroup]="tareaForm">
  Nombre: <input type="text" formControlName="nombre" required>
  <span [hidden]="tareaForm.get('nombre').valid" class="required-msj">Requerido</span>
  <br>
  <br>
  Descripción: <input type="text" formControlName="descripcion">
  <br>
  <br>
  <button (click)="cancelar()">Cancelar</button>
  &nbsp;
  <button type="submit" [disabled]="!tareaForm.valid">Guardar</button>
</form>
```

Ahora agregaremos una nueva ruta y modificaremos el link de editar.

Nuestro ruteo se vera algo así:

```ts
/// ...
}, {
  path: 'tareas',
  component: TareasComponent,
}, {
  path: 'tareas/:id',
  component: FormTareaComponent,
}, {
  path: 'tareas-r/:id',
  component: FormTareaRComponent,
}, {
/// ...
```

Y podemos agregar un nuevo link o modificar el que ya esta de editar, esto sera en la ruta de **Tareas**:

```html
<!-- ... -->
<td>
  <a [routerLink]="[tarea.id]">Editar</a> |
  <a [routerLink]="['/tareas-r', tarea.id]">Editar R</a> |
</td>
<!-- ... -->
```

## Observables y programación reactiva

La programación reactiva es un paradigma enfocado en el trabajo con flujos de datos finitos o infinitos de manera asíncrona. Es un estilo de microarquitectura y paradigma que implica el enrutamiento inteligente y el consumo de eventos, todos combinados, para cambiar el comportamiento de una aplicación.

El objetivo de la programación reactiva son:

1. Propagar los cambios en un sistema requiriendo la menor cantidad de esfuerzo.
2. Hacer más sencillo el trabajar con flujos asíncronos de datos.

Ejemplo de implementación:

![Ejemplo](imagenes/excel.gif)

![ReactiveX](imagenes/reactiveX.png)

ReactiveX es una librería para componer programas asincrónicos y basados en eventos mediante el uso de secuencias observables.

Extiende el patrón Observador para admitir secuencias de datos y / o eventos. Además propone operadores que permiten componer secuencias de forma declarativa; mientras abstrae el subprocesamiento de bajo nivel como el threading, sincronización, seguridad de subprocesos, estructuras de datos concurrentes y E/S no bloqueante.

Disponible para los siguientes lenguajes:

- Java: RxJava
- JavaScript: RxJS
- C#: Rx.NET
- C#(Unity): UniRx
- Scala: RxScala
- Clojure: RxClojure
- C++: RxCpp
- Lua: RxLua
- Ruby: Rx.rb
- Python: RxPY
- Go: RxGo
- Groovy: RxGroovy
- JRuby: RxJRuby
- Kotlin: RxKotlin
- Swift: RxSwift
- PHP: RxPHP
- Elixir: reaxive
- Dart: RxDart

### RxJS en Angular

RxJS es una biblioteca para componer programas asíncronos y basados ​​en eventos mediante el uso de secuencias observables. Porovee una implementación de el tipo **Obsevable**, el cual es necesario hasta que se implmente de forma nativa en JS y los navegadores lo soporten.

Los conceptos clave en RxJS son:

- **Observable**: Representa la idea de una colección invocable de valores o eventos futuros.
- **Observer**: es una colección de callbacks que sabe escuchar los valores entregados por el Observable.
- **Subscription**: Representa la ejecución de un observable, es principalmente útil para cancelar la ejecución.
- **Operators**: son funciones puras que permiten un estilo de programación funcional para manejar colecciones con operaciones como map, filter, concat, flatMap, etc.
- **Subject**: es el equivalente a un EventEmitter, y la única forma de multidifundir un valor o evento a múltiples Observadores.
- **Schedulers**: son despachadores centralizados para controlar la concurrencia,por ejemplo, nos permite coordinar cuándo se realiza el cálculo.

### Observables

Los observables proporcionan soporte para pasar mensajes entre publicadores y suscriptores. Los observables ofrecen beneficios significativos sobre otras técnicas para el manejo de eventos, la programación asíncrona y el manejo de múltiples valores.

Los observables son declarativos, es decir, se define una función para la publicación de valores, pero no se ejecuta hasta que un consumidor se suscribe a ella. El consumidor suscrito recibe notificaciones hasta que la función se completa o hasta que se da de baja.

Un observable tiene tres tipo de notificaciones:

- **next**: Un manejador por cada valor entregado. Llamado cero o más veces después de que comience la ejecución.
- **error**: Es opcional y es un manejador de errores el cual detiene la ejecución del observable.
- **complete**: Es opcional y es un manejador para la notificación de ejecución completada.

### Suscripción

Una instancia observable comienza a publicar valores solo cuando alguien se suscribe a ella. Para esto se us la llamando al método **subscribe()** de la instancia, pasando un objeto observador para recibir las notificaciones.

### Ejemplo

Agregar la siguiente funcionalidad:

- Ver en el menu el total de tareas creadas.
- Actualizar el total de tareas cada que se agregue una nueva.

Para lo anterior, agregaremos en el servicio de tareas un observable para el conteo, así como, la función para consumirlo:

```ts
/// ...
import { BehaviorSubject } from 'rxjs';
/// ...
export class TareasService {
  private lista: Tarea[] = [];
  private id = 0;

  private tareasTotales$ = new BehaviorSubject(0);
  /// ...
  public tareasTotales(): Observable<number> {
    return this.tareasTotales$.asObservable();
  }
}
```

Lo siguiente es agregar en cada método el conteo requerido:

```ts
export class TareasService {
  /// ...
  public agregar(tarea: Tarea): void {
    this.id++;
    tarea.id = this.id;
    this.lista.push(tarea);
    this.tareasTotales$.next(this.lista.length);
  }
  /// ...
}
```

Ahora vamos a implementar el servicio en el componente de navegación y a consumir el observable mediante una subscripción.

```ts
/// ...
export class NavegacionComponent implements OnInit {

  public menu: { nombre: string, ruta: string }[];

  public totalTareas: number;

  constructor(private tareasService: TareasService) {
    this.menu = [{
      nombre: 'inicio',
      ruta: ''
    }, {
      nombre: 'Tareas',
      ruta: '/tareas',
    }];
  }

  ngOnInit() {
    this.tareasService.tareasTotales().subscribe( total => {
      this.totalTareas = total;
    });
  }
}
```

En el caso del HTML, agregaremos una interpolacion de la variable:

```html
<ul>
  <li *ngFor="let m of menu">
    <a [routerLink]="m.ruta">{{m.nombre}}</a>
  </li>
  <li>
    Total de tareas: {{totalTareas}}
  </li>
</ul>
```

## Angular y consumo servicios web RESTful

### Qué es un servio web

Un web service es un programa diseñado para el intercambio de información maquina a maquina, sobre una red. Entonces esto hace que una computadora o programa pueda solicitar y recibir información de otra computadora o programa. A quien solicita la información se le llama cliente y a quien envía la información se le llama servidor.

### Que es un Api

La palabra viene de **Application Programming Interface**, y no es más que un programa que permite que otros programas se comuniquen con un programa en especifico. A diferencia de los web services, las API no necesariamente deben comunicarse entre una red, pueden usarse entre dos aplicaciones en una misma computadora.

### REST y RESTful

REST es una arquitectura para aplicaciones basadas en redes (como Internet), sus siglas significan **REpresentational State Transfer**. Un Servicio RESTful o RESTful Api, son programas basados en REST.

### Que hace que un servicio web sea REST

Un servicio web es REST cuando:

- No guarda estado, esto es gracias a que cada petición tiene la información necesaria para ejecutarse.
- Usa los metodos HTTP de manera explicita
  - **`GET`**: Lee o lista
  - **`POST`**: Crea
  - **`PUT`**: Actualiza
  - **`DELETE`**: Elimina
- Usualmente regresan la información en formato JSON o XML.

  ```json
  [{
    usuario: 'joe',
    admin: true,
  }, {
    usuario: 'luis',
    admin: false
  }]
  ```

- Retornan códigos HTTP, por ejemplo:
  - **`200`**: Ok
  - **`400`**: Petición erronea
  - **`401`**: Sin autorización
  - **`404`**: Recurso no encontrado
- Expone las URIs en forma de directorio
  - `http://webapp.somee.com/api/tareas`
  - `http://webapp.somee.com/api/tareas/1`

### Consumir servicio webs

Para el ejemplo vamos a configurar IIS y a registrar una webapi.

#### Instalación de IIS

Para instalar **`IIS`** vamos al **`Panel de control`** > **`Programas`** > **`Activar o desactivar características de Windows`**

![IIS 01](imagenes/iis01.png)

Aparecera una ventana donde se cargara las características disponibles, en esta lista vamos a buscar **`Intenet Information Services`**, la expandiremos y vamos a seleccionar la consola.

![IIS 02](imagenes/iis02.png)

Ahora expandiremos **`Application Development Features`** y seleccionaremos la opción **`ASP.Net 3.5 y ASP.Net 4.7`**

![IIS 03](imagenes/iis03.png)

Por último expandiremos **`Common HTTP Features`** y seleccionaremos las tres opciones de la imagen.

![IIS 04](imagenes/iis04.png)

Hecho lo anterior, daremos aceptar y esperaremos a que se instalen las características configuradas.

![IIS 05](imagenes/iis05.png)

> Si es necesario, reinicien su equipo.

Para comprobar el funcionamiento de **`IIS`**, abriremos nuestro navegador y nos dirijiremos a **`http://localhost`**.

#### Registrar webapi

Vamos a descomprimir el archivo **`webapi.zip`** y a pegar el contenido en **`C:\inetpub\wwwroot`**; esta es la ruta predeterminada para las apliaciones web de **`IIS`**.

![webapi 01](imagenes/webapi01.png)

Buscaremos en el buscador del menu de windows **`IIS`** y lo abriremos.

![webapi 02](imagenes/webapi02.png)

Vamos a expandir el contenido del lado izquierdo y daremos clic derecho en la carpeta **`webapi`**

![webapi 03](imagenes/webapi03.png)

Aparecera un recuadro de configuración, solo daremos clic en aceptar y nuestra webapi estara lista para usarse.

![webapi 04](imagenes/webapi04.png)

Podemos probar el funcionamiento del api mediante **`Swagger`** entrando a la url **`http://localhost/webapi/swagger`**.

![webapi 05](imagenes/webapi05.png)

> **`Swagger`** es una libreria que se instala por medio de **`NuGet`** y dota al webapi de una interfaz gráfica para probar los metodos de este.

#### Consumo de webapi desde Angular

Para consumir apis RESTful lo primero que tenemos que hacer es importar  **`HttpClientModule`** en el modulo root.

```ts
/// ...
import { HttpClientModule } from '@angular/common/http';
/// ...
@NgModule({
  /// ...
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  /// ...
})
export class AppModule { }
// ...
```

En el servicio **`TareasService`** haremos lo siguiente:

- Agregar la dependencia de **`HttpClient`** en el constructor.
- Eliminar la variable para controlar el id y las tareas.
- Quitar la inicialización de la lista de tareas.
- Agregar una variable con el header para las peticiones.
- Declara una variable con la url base del webapi.
- Hacer publico **`tareasTotales$`** para actualizar el total.
- Remplazar el contenido de cada función con el metodo del webapi que le corresponde.

```ts
/// ...
import { HttpClient } from '@angular/common/http';
/// ...
export class TareasService {
  public tareasTotales$ = new BehaviorSubject(0);
  private url = 'http://localhost/webapi/api/Tareas';
  private requestOptions = {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
  };

  constructor(private httpClient: HttpClient) { }

  public listar(): Observable<Tarea[]> {
    return this.httpClient.get<Tarea[]>(this.url, this.requestOptions);
  }

  public obtener(id: number): Observable<Tarea> {
    return this.httpClient.get<Tarea>(`${this.url}/${id}`, this.requestOptions);
  }

  public agregar(tarea: Tarea): Observable<string> {
    return this.httpClient.post<string>(this.url, tarea, this.requestOptions);
  }

  public actulizar(tarea: Tarea): Observable<string> {
    return this.httpClient.put<string>(this.url, tarea, this.requestOptions);
  }

  public tareasTotales(): Observable<number> {
    return this.tareasTotales$.asObservable();
  }
}
```

Lo siguiente es modificar el **`ngOnInit`** del componente de **`Tareas`**, nos suscribirnos al servicio de listar:

```ts
ngOnInit() {
  this.tareasService.listar().subscribe(tareas => {
    this.tareas = tareas;
    this.tareasService.tareasTotales$.next(tareas.length);
  });
}
```

En el caso de los formulario, tambien es necesario agregar la suscripción a los metodos.

Formulario Template-Drive:

```ts
/// ...
export class FormTareaComponent implements OnInit {

  public tarea: Tarea;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private tareaServices: TareasService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((param: any) => {
      const id = parseInt(param.id, 10);
      this.tarea = new Tarea();

      if (id !== 0) {
        this.tareaServices.obtener(id).subscribe(tarea => {
          this.tarea = tarea;
        });
      }
    });
  }

  public guardar(e: any): void {
    let peticion: Observable<any>;

    if (!this.tarea.id) {
      peticion = this.tareaServices.agregar(this.tarea);
    } else {
      peticion = this.tareaServices.actulizar(this.tarea);
    }

    peticion.subscribe(res => {
      this.router.navigate(['tareas']);
    });
  }

  public cancelar(): void {
    this.router.navigate(['tareas']);
  }
}
```

## Publicar una aplicación en IIS

Para publicar nuestra aplicación tendremos que compilarla en modo producción y como usaremos **`IS`**, asignarle una ruta de aplicación.

```bash
ng build --prod --base-href="my-app/"
```

El comando anterior nos va a generar el directorio **`dist`**, el cual contiene nuestra aplicación angular. Lo que tendremos que hacer con esta es copiarla a la ruta de las aplicaciones **`IIS`**.

> Despues de que se genere el index.html agregaremos un '/' al href del bese. **`href="/my-app/"`**

![Publicar 01](imagenes/publicar01.png)

Despues abriremos la consola de **`IIS`** y convertiremos nuestro directorio en una aplicación.

![Publicar 02](imagenes/publicar02.png)

Nuestra aplicación deberias estar funcionando en la ruta **`http://localhost/my-app/`**.

### Configurar routing de IIS para aplicaciones Angular

Para que el routing de angular funcione necesitamos dos cosas:

1. Instalar el Rewrite Module d **`IIS`**, que se utiliza para modificar a las rutas que debe responder el servidor.

   > Usar el instalador

2. Agregar un archivo **`web.config`** al directorio de la aplicación Angular con la siguiente estructura:

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <configuration>
    <system.webServer>
      <rewrite>
        <rules>
          <rule name="Angular Routes" stopProcessing="true">
            <match url=".*" />
            <conditions logicalGrouping="MatchAll">
              <add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />
              <add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true" />
            </conditions>
            <action type="Rewrite" url="./index.html" />
          </rule>
        </rules>
      </rewrite>
    </system.webServer>
  </configuration>
  ```
